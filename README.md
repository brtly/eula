# EULA

This EULA ("Agreement") constitutes a legal Agreement between you ("User") and Bertly ("Bertly") and acts as a supplement to the [terms of use of the Atlassian Marketplace](https://www.atlassian.com/licensing/marketplace/termsofuse). It governs the User's download and/or use of any of Bertly's apps in the Atlassian Marketplace ("Software"), including any update or upgrade to it. Unless otherwise indicated, the Software is property of Bertly. By accessing or using the Software, the User signifies that he/she has read, understands, and agrees to be legally bound by this Agreement.

Bertly reserves the right to update or modify this Agreement at
any time without further notice. Your continued use of the Software after any changes constitutes your acceptance of the Agreement. If Bertly modifies the Agreement during the User’s license or subscription term, the modified version will be effective upon the User’s next renewal of the license or subscription term, as applicable. In this case, if the User objects to the updated Agreement, the User may choose not to renew.

## Intellectual property

Unless otherwise indicated, the Software is property of Bertly. All rights reserved.

## Warranty disclaimer

NO WARRANTY: THE USER EXPRESSLY ACKNOWLEDGES AND AGREES THAT USE OF THE SOFTWARE IS AT ITS SOLE RISK. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE SOFTWARE AND ANY SERVICES PERFORMED OR PROVIDED BY BERTLY ARE PROVIDED "AS IS" AND "AS AVAILABLE", WITH ALL FAULTS AND WITHOUT WARRANTY OF ANY KIND, AND BERTLY HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS WITH RESPECT TO THE SOFTWARE AND ANY SERVICES, EITHER EXPRESS, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES AND/OR CONDITIONS OF MERCHANTABILITY, OF SATISFACTORY QUALITY, OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY, AND OF NONINFRINGEMENT OF THIRD-PARTY RIGHTS. NO ORAL OR WRITTEN INFORMATION OR ADVICE GIVEN BY BERTLY OR ITS AUTHORIZED REPRESENTATIVES SHALL CREATE A WARRANTY. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES OR LIMITATIONS ON APPLICABLE STATUTORY RIGHTS OF A CONSUMER, SO THE ABOVE EXCLUSION AND LIMITATIONS MAY NOT APPLY TO YOU.

## Limitation of liability

TO THE EXTENT NOT PROHIBITED BY LAW, IN NO EVENT SHALL BERTLY BE LIABLE FOR PERSONAL INJURY OR ANY INCIDENTAL, SPECIAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, LOSS OF DATA, BUSINESS INTERRUPTION, OR ANY OTHER COMMERCIAL DAMAGES OR LOSSES, ARISING OUT OF OR RELATED TO YOUR USE OF OR INABILITY TO USE THE SOFTWARE, HOWEVER CAUSED, REGARDLESS OF THE THEORY OF LIABILITY AND EVEN IF BERTLY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OF LIABILITY FOR PERSONAL INJURY, OR OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THIS LIMITATION MAY NOT APPLY TO YOU.

## Indemnification

The use of the Software constitutes the User's agreement to defend, indemnify, and hold harmless Bertly and his respective employees, contractors, officers, directors, licensees (excluding the User), sublicensees, successors and assigns from and against any actions, claims, demands, liability and expenses, including reasonable attorneys fees, arising out of (i) the User's breach of any of
this Agreement; (ii) the User's use or misuse of the Software, or
(iii) the use or misuse of the Software by a third party using it.

## Severability

If any part of this Agreement is held invalid or unenforceable, that portion shall be interpreted in a manner consistent with applicable law to reflect, as nearly as possible, the original intentions of Bertly, and the remaining portions shall remain in full force and effect. The same applies to loopholes and omissions.

## Waiver

The failure of Bertly to exercise or enforce any right or provision of this Agreement will not constitute waiver of such right or provision.

## Governing law and jurisdication

This Agreement shall be applied, governed by and construed in accordance with German law. Any dispute in connection with this Agreement shall be submitted to the exclusive jurisdiction of German general courts. Any court proceeding shall be initiated by filing with the District Court of Stuttgart (Amtsgericht Stuttgart).

